# TMD DatabaseHelper


## Composer
Package is installable via composer, taging and pushing
Change in composer version
```
git tag v0.0.x
git push origin v0.0.x
```


Contains following connections:
* MySQLi
* SQL Server (MSSQL)
* ODBC
* SASQL SQLAnywhere (Sybase)
* SQLite3



## Install via composer
```
composer config repositories.repo-name git https://bitbucket.org/tmdevelopment/databasehelper.git
#composer config minimum-stability dev
composer require tmdevelopment/databasehelper:dev-master
```

## Usage
```

use DatabaseHelper\MSSQL;
use DatabaseHelper\MySQLi;
use DatabaseHelper\SASQL;
use DatabaseHelper\ODBC;
use DatabaseHelper\SQLiteV3;


use DatabaseHelper\MSSQL;
use DatabaseHelper\MySQLi;
use DatabaseHelper\SASQL;
use DatabaseHelper\ODBC;
use DatabaseHelper\SQLiteV3;


$mssql = new MSSQL("192.168.88.41\SQLEXPRESS01,1433", "malmerk_integration", "****", "DWdata");
$mssql->setQuery("SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES");
var_dump($mssql->loadObjectList());

$mysql = new MySQLi("localhost", "root", "****", "INFORMATION_SCHEMA");
$mysql->setQuery("SELECT TABLE_SCHEMA, TABLE_NAME FROM INFORMATION_SCHEMA.TABLES LIMIT 10");
var_dump($mysql->loadObjectList());

$sasql = new SASQL("erpg5", "MonitorG5_erpg5", "013", "dba", "****");
$sasql->setQuery("SELECT su.user_name AS TABLE_SCHEMA, st.TABLE_NAME FROM sys.SYSTABLE st JOIN sys.sysuser su ON st.creator = su.user_id WHERE su.user_name != 'SYS' ");
var_dump($sasql->loadObjectList());

$odbc = new ODBC("Driver={SQL Anywhere 17};Host=erpg4:2638;Server=monwin;Database=FTG_001;","monodbc","****");
$odbc->setQuery("SELECT su.user_name AS TABLE_SCHEMA, st.TABLE_NAME FROM sys.SYSTABLE st JOIN sys.sysuser su ON st.creator = su.user_id WHERE su.user_name != 'SYS' ");
var_dump($odbc->loadObjectList());

$odbc = new ODBC("001","monodbc","****");
$odbc->setQuery("SELECT su.user_name AS TABLE_SCHEMA, st.TABLE_NAME FROM sys.SYSTABLE st JOIN sys.sysuser su ON st.creator = su.user_id WHERE su.user_name != 'SYS' ");
var_dump($odbc->loadObjectList());


$sqlite3 = new SQLiteV3("test.db");
$sqlite3->setQuery("SELECT * FROM sqlite_master WHERE type='table'");
var_dump($sqlite3->loadObjectList());

```