<?php

namespace DatabaseHelper;

/**
 * TMDevelopment ODBC class
 *
 * @package    tmdevelopment.databasehelper
 * @license		GNU/GPL
 */
class ODBC extends Connection
{
    var $function_prefix = "odbc";

    //Connection params
    var $dsn = "";
    var $user = "";
    var $pass = "";
    var $db = "";
    var $prefix = "";

    /**
     * Cunstructor of ODBC class
     *
     * @param string $dsn Datasource name like 001 or "Driver={SQL Server Native Client 10.0};Server=$server;Database=$database;"
     * @param string $user ODBC connection username
     * @param string $pass ODCB connection password
     * @param string $db database name
     * @param string $prefix table prefix
     *
     * @access public
     */
    function __construct($dsn = null, $user = null, $pass = null, $prefix = null)
    {
        if (!function_exists("odbc_connect")) {
            throw new Exception("function odbc_connect does not exist");
            exit();
        }
        if (isset($dsn)) {
            $this->dsn = $dsn;
        }
        if (isset($user)) {
            $this->user = $user;
        }
        if (isset($pass)) {
            $this->pass = $pass;
        }

        if (isset($prefix)) {
            $this->prefix = $prefix;
        }
        $this->isConnected = $this->connect();
    }

    /**
     *
     * @return boolean
     *
     * @access private
     */
    private function connect()
    {

        $result = true;

        if (empty($this->connection)) {
            set_error_handler(array(parent::class, 'custom_error_handler'));
            try {
                $this->connection = odbc_connect($this->dsn, $this->user, $this->pass);
            } catch (Exception $e) {
                $this->error_msg = $e->getMessage();
                $this->error = -1;
                $result = false;
            }
            restore_error_handler();
        }
        return $result;
    }



    /**
     * Method for querying previously set query
     *
     * @access public
     * @return boolean result of querying
     */
    public function query()
    {
        if ($this->connection) {
            $this->error_msg = null;
            $this->result = odbc_exec($this->connection, $this->query);
            if (!$this->result) {
                $this->error_msg = odbc_errormsg($this->connection);
                $this->error = odbc_error($this->connection);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method for loading all the rows as array of objects
     *
     * @access public
     * @return mixed, false if query failed, on success array
     */
    public function loadObjectList()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = odbc_fetch_object($this->result)) {
                $returnArray[] = $row;
            }
            odbc_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadAssocList()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = odbc_fetch_array($this->result)) {
                $returnArray[] = $row;
            }
            odbc_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadResultArray()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = odbc_fetch_row($this->result)) {
                $returnArray[] = $row[0];
            }
            odbc_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    /**
     * Method for loading first value of query statement. e.g. "SELECT 1, 2, 3" 1 will be returned
     *
     * @return mixed, boolean false if query fails, on success first value
     */
    public function loadResult()
    {
        if ($this->query()) {
            return odbc_fetch_row($this->result)[0];
        }
        return false;
    }

    /**
     * Method for loading single object (row)
     *
     * @access public
     * @return mixed, boolean false if query fails or row count does not equal 1, on success object
     */
    public function loadObject()
    {
        if ($this->query()) {
            if (empty($this->error_msg) && odbc_num_rows($this->result) != 1) {
                if (odbc_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (odbc_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = odbc_fetch_object($this->result);
            return $ret;
        }
        return false;
    }


    /**
     * Method for loading single array (row)
     *
     * @access public
     * @return mixed, boolean false if query fails or row count does not equal 1, on success object
     */
    public function loadAssoc()
    {
        if ($this->query()) {
            if (empty($this->error_msg) && odbc_num_rows($this->result) != 1) {
                if (odbc_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (odbc_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = odbc_fetch_array($this->result);
            return $ret;
        }
        return false;
    }

    public function printHTMLTable()
    {
        $this->query();
        if ($this->result && odbc_num_rows($this->result)) {
            $nrow = 0;
            echo ("<table border=1 cellpadding=5 cellspacing=0><tr><th>Row</th>\n");
            //$finfo = mysqli_fetch_fields($this->result);
            while ($row = odbc_fetch_array($this->result)) {


                if ($nrow == 0) {
                    foreach ($row as $key => $val) {
                        echo ("<th>" . $key . "</th>\n");
                    }
                    echo ("</tr>\n");
                }





                echo ("<tr><td>" . $nrow . "</td>");
                foreach ($row as $key => $val) {
                    echo ("<td>" . $val . "</td>\n");
                }
                echo ("</tr>\n");
                $nrow++;
            }
            echo ("</table>\n");
        } else {
            echo "<p>No data found</p>";
        }
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->connection);
    }

    public function insertObject($table, &$object, $keyName = null)
    {
        return $this->insertOrUpdateObject($table, $object, $keyName, null, false);
    }

    public function insertOrUpdateObject($table, &$object, $keyName = null, $doNotUpdate = array() /* fields that will not be updated */, $update = true)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES ( %s ) ' . ($update ? ' ON DUPLICATE KEY UPDATE' : '');
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $values[] = $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(",", $fields), implode(",", $values));

        if ($update) {
            // quotes values
            for ($j = 0; $j < count($doNotUpdate); $j++) {
                $doNotUpdate[$j] = $this->nameQuote($doNotUpdate[$j]);
            }
            for ($i = 0; $i < count($fields); $i++) {
                if (!in_array($fields[$i], $doNotUpdate)) {
                    if ($i != 0) {
                        $fmtsql = $fmtsql . ",";
                    }
                    $fmtsql = $fmtsql . " " . $fields[$i] . "=" . $values[$i];
                }
            }
        }

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            if ($this->debug) {
                var_dump($this->query, $this->error_msg);
            }
            return false;
        }
        if ($this->getAffectedRows() > 0 && empty($object->$keyName) && !empty($keyName)) {
            $object->$keyName = $this->insertId();
        }
        return true;
    }

    public function insertId()
    {
        return mysqli_insert_id($this->connection);
    }

    public function bulkInsertOrUpdateObject($table, $objects, $objectsPerQuery = 100)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES (%s) ON DUPLICATE KEY UPDATE %s';
        $fields = array();
        $update_fields = array();
        foreach (get_object_vars($objects[0]) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $update_fields[] = $this->nameQuote($k) . "=VALUES(" . $this->nameQuote($k) . ") ";
        }
        $values_arr = array();
        $mitmes = 0;
        foreach ($objects as $obj) {
            $mitmes++;
            $values = array();
            foreach (get_object_vars($obj) as $k => $v) {
                if (is_array($v) or is_object($v)/* or $v === NULL */) {
                    continue;
                }
                if ($k[0] == '_') { // internal field
                    continue;
                }
                $values[] = $this->Quote($v);
            }
            $values_arr[] = join(",", $values);
            if ($mitmes == $objectsPerQuery) {
                $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
                $this->setQuery($sql);
                if (!$this->query()) {
                    var_dump($this);
                    echo $sql;
                }
                $mitmes = 0;
                $values_arr = array();
            }
        }
        if ($mitmes != 0 && $mitmes != $objectsPerQuery) {
            $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
            $this->setQuery($sql);
            if (!$this->query()) {
                var_dump($this);
                echo $sql;
            }
        }
    }

    public function updateObject($table, &$object, $keyName = null)
    {
        $fmtsql = 'UPDATE ' . $this->nameQuote($table) . ' SET %s  WHERE ' . $this->nameQuote($keyName) . ' = ' . (is_numeric($object->$keyName) ? $object->$keyName : $this->quote($object->keyName));
        $fieldValuePairs = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fieldValuePairs[] = $this->nameQuote($k) . "=" . $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(", ", $fieldValuePairs));
        $this->setQuery($fmtsql);
        if (!$this->query()) {
            var_dump($this->error_msg);
            return false;
        }
        return true;
    }

    public function setDebug($value)
    {
        $this->debug = (bool) $value;
    }

    public function getTableColumns($table, $schema)
    {
        return false;
    }
}
