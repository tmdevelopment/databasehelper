<?php

namespace DatabaseHelper;

/**
 * TMDevelopment SASQL class
 *
 * @package    tmdevelopment.databasehelper
 * @license		GNU/GPL
 */

//https://wiki.scn.sap.com/wiki/display/SQLANY/The+SAP+SQL+Anywhere+PHP+Module
//https://s3.amazonaws.com/sqlanywhere/drivers/php/sasql_php.zip
class SASQL extends Connection
{

    var $function_prefix = "sasql";

    //Connection params
    var $host = "";
    var $server = "";
    var $user = "";
    var $pass = "";
    var $db = "";
    var $file = null;

    /**
     * Cunstructor of SASQL class
     *
     * @param string $host SA hostname  eg localhost
     * @param string $user SA servername monwin (can be detected from servicename)
     * @param string $user SA username
     * @param string $pass SA password
     * @param string $db SA database name
     * @param string $prefix table prefix
     *
     * @access public
     */
    function __construct($host = null, $server = null, $db = null, $user = null,  $pass = null, $prefix = null, $file=null)
    {
        if (!function_exists("sasql_connect")) {
            throw new Exception("function sasql_connect does not exist");
            exit();
        }
        if (isset($host)) {
            $this->host = $host;
        }
        if (isset($server)) {
            $this->server = $server;
        }
        if (isset($db)) {
            $this->db = $db;
        }
        if (isset($user)) {
            $this->user = $user;
        }
        if (isset($pass)) {
            $this->pass = $pass;
        }


        if (isset($prefix)) {
            $this->prefix = $prefix;
        }


        if (isset($file)) {
            $this->file = $file;
        }

        $this->isConnected = $this->connect();
    }

    /**
     *
     * @return boolean
     *
     * @access private
     */
    private function connect()
    {

        $result = true;

        if (empty($this->connection)) {
            set_error_handler(array($this, 'custom_error_handler'));
            try {
                if(!empty($this->file )){
                    $connectionString = sprintf("DBF=%s;UserID=%s;PWD=%s;DBN=%s", $this->file, $this->user, $this->pass, $this->db);
                }else{
                    $connectionString = sprintf("Host=%s;Server=%s;UserID=%s;PWD=%s;DBN=%s", $this->host . ":2638", $this->server, $this->user, $this->pass, $this->db);
                }
                $connectionString.=";CharSet=UTF-8";
                $this->connection = sasql_connect($connectionString);
            } catch (Exception $e) {
                $this->error_msg = $e->getMessage();
                $this->error = -1;
                $result = false;
            }
            restore_error_handler();
        }
        return $result;
    }

    /**
     * Method for querying previously set query
     *
     * @access public
     * @return boolean result of querying
     */
    public function query()
    {
        if ($this->connection) {
            $this->error_msg = null;
            $this->result = sasql_query($this->connection, $this->query);
            if (!$this->result) {
                $this->error_msg = sasql_error($this->connection);
                $this->error = sasql_errorcode($this->connection);
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method for loading all the rows as array of objects
     *
     * @access public
     * @return mixed, false if query failed, on success array
     */
    public function loadObjectList()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = sasql_fetch_object($this->result)) {
                $returnArray[] = $row;
            }
            sasql_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadAssocList()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = sasql_fetch_array($this->result, SASQL_ASSOC)) {
                $returnArray[] = $row;
            }
            sasql_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadResultArray()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = sasql_fetch_row($this->result)) {
                $returnArray[] = $row[0];
            }
            sasql_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    /**
     * Method for loading first value of query statement. e.g. "SELECT 1, 2, 3" 1 will be returned
     *
     * @return mixed, boolean false if query fails, on success first value
     */
    public function loadResult()
    {
        if ($this->query()) {
            return sasql_fetch_row($this->result)[0];
        }
        return false;
    }

    /**
     * Method for loading single object (row)
     *
     * @access public
     * @return mixed, boolean false if query fails or row count does not equal 1, on success object
     */
    public function loadObject()
    {
        if ($this->query()) {
            if (empty($this->error_msg) && sasql_num_rows($this->result) != 1) {
                if (sasql_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (sasql_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = sasql_fetch_object($this->result);
            return $ret;
        }
        return false;
    }


    /**
     * Method for loading single array (row)
     *
     * @access public
     * @return mixed, boolean false if query fails or row count does not equal 1, on success object
     */
    public function loadAssoc()
    {
        if ($this->query()) {
            if (empty($this->error_msg) && sasql_num_rows($this->result) != 1) {
                if (sasql_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (sasql_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = sasql_fetch_array($this->result, SASQL_ASSOC);
            return $ret;
        }
        return false;
    }

    public function printHTMLTable()
    {
        $this->query();
        if ($this->result && sasql_num_rows($this->result)) {
            $nrow = 0;
            echo ("<table border=1 cellpadding=5 cellspacing=0><tr><th>Row</th>\n");
            //$finfo = mysqli_fetch_fields($this->result);
            while ($row = sasql_fetch_array($this->result, SASQL_ASSOC)) {


                if ($nrow == 0) {
                    foreach ($row as $key => $val) {
                        echo ("<th>" . $key . "</th>\n");
                    }
                    echo ("</tr>\n");
                }





                echo ("<tr><td>" . $nrow . "</td>");
                foreach ($row as $key => $val) {
                    echo ("<td>" . $val . "</td>\n");
                }
                echo ("</tr>\n");
                $nrow++;
            }
            echo ("</table>\n");
        } else {
            echo "<p>No data found</p>";
        }
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->connection);
    }

    public function insertObject($table, &$object, $keyName = null)
    {
        return $this->insertOrUpdateObject($table, $object, $keyName, null, false);
    }

    public function insertOrUpdateObject($table, &$object, $keyName = null, $doNotUpdate = array() /* fields that will not be updated */, $update = true)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES ( %s ) ' . ($update ? ' ON DUPLICATE KEY UPDATE' : '');
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $values[] = $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(",", $fields), implode(",", $values));

        if ($update) {
            // quotes values
            for ($j = 0; $j < count($doNotUpdate); $j++) {
                $doNotUpdate[$j] = $this->nameQuote($doNotUpdate[$j]);
            }
            for ($i = 0; $i < count($fields); $i++) {
                if (!in_array($fields[$i], $doNotUpdate)) {
                    if ($i != 0) {
                        $fmtsql = $fmtsql . ",";
                    }
                    $fmtsql = $fmtsql . " " . $fields[$i] . "=" . $values[$i];
                }
            }
        }

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            if ($this->debug) {
                var_dump($this->query, $this->error_msg);
            }
            return false;
        }
        if ($this->getAffectedRows() > 0 && empty($object->$keyName) && !empty($keyName)) {
            $object->$keyName = $this->insertId();
        }
        return true;
    }

    public function insertId()
    {
        return mysqli_insert_id($this->connection);
    }

    public function bulkInsertOrUpdateObject($table, $objects, $objectsPerQuery = 100)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES (%s) ON DUPLICATE KEY UPDATE %s';
        $fields = array();
        $update_fields = array();
        foreach (get_object_vars($objects[0]) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $update_fields[] = $this->nameQuote($k) . "=VALUES(" . $this->nameQuote($k) . ") ";
        }
        $values_arr = array();
        $mitmes = 0;
        foreach ($objects as $obj) {
            $mitmes++;
            $values = array();
            foreach (get_object_vars($obj) as $k => $v) {
                if (is_array($v) or is_object($v)/* or $v === NULL */) {
                    continue;
                }
                if ($k[0] == '_') { // internal field
                    continue;
                }
                $values[] = $this->Quote($v);
            }
            $values_arr[] = join(",", $values);
            if ($mitmes == $objectsPerQuery) {
                $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
                $this->setQuery($sql);
                if (!$this->query()) {
                    var_dump($this);
                    echo $sql;
                }
                $mitmes = 0;
                $values_arr = array();
            }
        }
        if ($mitmes != 0 && $mitmes != $objectsPerQuery) {
            $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
            $this->setQuery($sql);
            if (!$this->query()) {
                var_dump($this);
                echo $sql;
            }
        }
    }

    public function updateObject($table, &$object, $keyName = null)
    {
        $fmtsql = 'UPDATE ' . $this->nameQuote($table) . ' SET %s  WHERE ' . $this->nameQuote($keyName) . ' = ' . (is_numeric($object->$keyName) ? $object->$keyName : $this->quote($object->keyName));
        $fieldValuePairs = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fieldValuePairs[] = $this->nameQuote($k) . "=" . $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(", ", $fieldValuePairs));
        $this->setQuery($fmtsql);
        if (!$this->query()) {
            var_dump($this->error_msg);
            return false;
        }
        return true;
    }

    public function getTableColumns($table, $schema)
    {
        $result = array();
        $query = "SELECT su.user_name AS schema, sc.*
                    FROM sys.SYSTABLE st
                    JOIN sys.sysuser su ON st.creator = su.user_id
                    JOIN sys.syscolumns sc ON sc.tname = st.table_name
                    WHERE schema = " . $this->quote($schema) . " AND tname = " . $this->quote($table);
        $this->setQuery($query);
        $columns = $this->loadObjectList();
        foreach ($columns as $column) {
            $result[] = (object) array(
                "TABLE_CATALOG" => "",
                "TABLE_SCHEMA" => $column->schema,
                "TABLE_NAME" => $table,
                "COLUMN_NAME" => $column->cname,
                "ORDINAL_POSITION" => $column->colno,
                "COLUMN_DEFAULT" => $column->default_value,
                "IS_NULLABLE" => $column->nulls == "Y" ? "YES" : "NO",
                "DATA_TYPE" => $column->coltype,
                "CHARACTER_MAXIMUM_LENGTH" => $column->length,
                "CHARACTER_OCTET_LENGTH" => "",
                "NUMERIC_PRECISION" => "",
                "NUMERIC_SCALE" => "",
                "DATETIME_PRECISION" => "",
                "CHARACTER_SET_NAME" => "",
                "COLLATION_NAME" => "",
                "COLUMN_TYPE" => "",
                "COLUMN_KEY" => "",
                "EXTRA" => "",
                "PRIVILEGES" => "",
                "COLUMN_COMMENT" => ""
            );
        }
        return $result;
    }
}
