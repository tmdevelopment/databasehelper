<?php

namespace DatabaseHelper;

/**
 * TMDevelopment MSSQL class
 *
 * @package    tmdevelopment.databasehelper
 * @license		GNU/GPL
 */


abstract class Connection
{

    var $function_prefix;

    var $query;
    var $connection;
    var $result;
    var $error_msg;
    var $error;

    var $serverVersion;

    protected $isConnected;
    protected $debug = false;

    var $prefix = "";

    function __destruct()
    {
        if ($this->connection) {
            $this->close();
        }
    }

    protected function custom_error_handler($severity, $message, $file, $line)
    {
        throw new \ErrorException($message, $severity, $severity, $file, $line);
    }

    public function setQuery($query)
    {
        if (strlen($query)) {
            unset($this->error_msg);
            unset($this->error);
            $this->query = str_replace("#__", $this->prefix, $query);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param string $sql SQL query to be executed
     * @return boolean result of execution
     */
    public function execute($sql)
    {
        return $this->setQuery($sql) && $this->query();
    }



    /**
     * Method for quoting content of variables
     *
     * @param string $text
     * @access public
     * @return string quoted $text
     */
    public function quote($text)
    {
        $functions = array("NOW()");
        if (in_array($text, $functions)) {
            return $text;
        }
        return "'" . str_replace("'", "\'", $text) . "'";
    }

    public function quoteArray($arr)
    {
        foreach ($arr as $key => $val) {
            $arr[$key] = $this->quote($val);
        }
        return $arr;
    }

    public function escape($text, $extra = false)
    {
        if (is_int($text) || is_float($text)) {
            return $text;
        }
        $text = str_replace("'", "''", $text);
        return addcslashes($text, "\000\n\r\\\032");
    }

    public function nameQuote($name)
    {
        $name = explode(".", $name);
        foreach ($name as $namePart) {
            $namePart = $this->quote($namePart);
        }
        return implode(".", $name);
    }


    public function isConnected()
    {
        return $this->isConnected;
    }

    public function getErrorMessage()
    {
        return $this->error_msg;
    }

    public function close()
    {
        $function_name = $this->function_prefix . "_close";
        $function_name($this->connection);
    }

    public function getTableColumns($table, $schema)
    {
        $this->setQuery("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = " . $this->quote($schema) . " AND TABLE_NAME = " . $this->quote($table));
        return $this->loadObjectList();
    }
}
