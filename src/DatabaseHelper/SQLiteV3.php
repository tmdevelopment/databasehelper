<?php

namespace DatabaseHelper;

/**
 * TMDevelopment SQLite3 class
 *
 * @package    tmdevelopment.databasehelper
 * @license		GNU/GPL
 */
class SQLiteV3 extends Connection
{

    //Connection params
    private $path = "test.db";

    function __construct($path)
    {
        if (!class_exists("SQLite3")) {
            throw new  \Exception("class SQLite3 does not exist");
        }
        if (isset($path)) {
            $this->path = $path;
        }
        $this->connect();
    }


    public function connect()
    {
        if (empty($this->connection)) {
            set_error_handler(array(parent::class, "custom_error_handler"));
            try {
                $this->connection = new \SQLite3($this->path);
            } catch (Exception $e) {
                $this->error_msg = $e->getMessage();
                $this->error = -1;
            }
            restore_error_handler();
        }
        return true;
    }

    public function close()
    {
        $this->connection->close();
    }

    public function query()
    {
        $r = true;
        if ($this->connection) {
            $this->error_msg = null;
            set_error_handler(array(parent::class, "custom_error_handler"));
            try {
                $this->result = $this->connection->query($this->query);
            } catch (Exception $e) {
                $this->error_msg = $this->connection->lastErrorMsg();
                $this->error = $this->connection->lastErrorCode();
                $r = false;
            }
            restore_error_handler();
        } else {
            $r = false;
        }
        return $r;
    }

    public function loadObjectList()
    {
        if ($this->query()) {
            $returnArray = array();

            while ($row = $this->result->fetchArray(SQLITE3_ASSOC)) {
                $returnArray[] = (object) $row;
            }
            return $returnArray;
        }
        return false;
    }

    public function loadResult()
    {
        if ($this->query()) {
            return $this->result->fetchArray(SQLITE3_NUM)[0];
        }
        return false;
    }

    public function loadObject()
    {
        if ($this->query()) {
            if (empty($this->error_msg)) {

                $row = $this->result->fetchArray(SQLITE3_ASSOC);
                if (empty($row)) {
                    $this->error_msg = "Zero rows found";
                } else {


                    if ($this->result->fetchArray() === false) {
                        return (object) $row;
                    } else {
                        $this->error_msg = "More than 1 row found";
                    }
                }
            }
        }
        return false;
    }

    public function printHTMLTable()
    {
        $this->query();
        if ($this->result) {
            $nrow = 0;
            echo ("<table border=1 cellpadding=5 cellspacing=0><tr><th>Row</th>\n");


            $i = 0;
            while ($this->result->columnName($i)) {
                echo ("<th>" . $this->result->columnName($i) . "</th>\n");
                $i++;
            }



            echo ("</tr>\n");



            while (($row = $this->result->fetchArray(SQLITE3_NUM)) !== false) {
                echo ("<tr><td>" . $nrow . "</td>");
                foreach ($row as $column) {
                    echo ("<td>" . $column . "</td>\n");
                }
                echo ("</tr>\n");
                $nrow++;
            }
            echo ("</table>\n");
        } else {
            echo "<p>No data found</p>";
        }
    }

    public function getAffectedRows()
    {
        return $this->connection->changes();
    }

    public function insertObject($table, &$object, $keyName = null)
    {
        $this->insertOrUpdateObject($table, $object, $keyName, null, false);
    }

    private function insertOrUpdateObject($table, &$object, $keyName = null, $doNotUpdate = array() /* fields that will not be updated */, $update = true)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES ( %s ) ' . ($update ? ' ON DUPLICATE KEY UPDATE' : '');
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $values[] = $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(",", $fields), implode(",", $values));

        if ($update) {
            // quotes values
            for ($j = 0; $j < count($doNotUpdate); $j++) {
                $doNotUpdate[$j] = $this->nameQuote($doNotUpdate[$j]);
            }
            for ($i = 0; $i < count($fields); $i++) {
                if (!in_array($fields[$i], $doNotUpdate)) {
                    if ($i != 0) {
                        $fmtsql = $fmtsql . ",";
                    }
                    $fmtsql = $fmtsql . " " . $fields[$i] . "=" . $values[$i];
                }
            }
        }

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            var_dump($this->error_msg);
            return false;
        }
        if (!empty($keyName)) {
            $object->$keyName = $this->insertId();
        }
        return true;
    }

    private function bulkInsertOrUpdateObject($table, $objects, $objectsPerQuery = 100)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES (%s) ON DUPLICATE KEY UPDATE %s';
        $fields = array();
        $update_fields = array();
        foreach (get_object_vars($objects[0]) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $update_fields[] = $this->nameQuote($k) . "=VALUES(" . $this->nameQuote($k) . ") ";
        }
        $values_arr = array();
        $mitmes = 0;
        foreach ($objects as $obj) {
            $mitmes++;
            $values = array();
            foreach (get_object_vars($obj) as $k => $v) {
                if (is_array($v) or is_object($v)/* or $v === NULL */) {
                    continue;
                }
                if ($k[0] == '_') { // internal field
                    continue;
                }
                $values[] = $this->Quote($v);
            }
            $values_arr[] = join(",", $values);
            if ($mitmes == $objectsPerQuery) {
                $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
                $this->setQuery($sql);
                if (!$this->query()) {
                    var_dump($this);
                    echo $sql;
                }
                $mitmes = 0;
                $values_arr = array();
            }
        }
        if ($mitmes != 0 && $mitmes != $objectsPerQuery) {
            $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
            $this->setQuery($sql);
            if (!$this->query()) {
                var_dump($this);
                echo $sql;
            }
        }
    }

    public function updateObject($table, $object, $keyName)
    {

        $fmtsql = "UPDATE " . $this->nameQuote($table) . " SET %s WHERE $keyName = " . $object->$keyName;

        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null or $k == $keyName) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }

            $fields[] = $this->nameQuote($k) . "=" . $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, join(",", $fields));

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            var_dump($this->error_msg);
            return false;
        }
        return true;
    }

    public function insertId()
    {
        return $this->connection->lastInsertRowID();
    }

    public function getTableColumns($table, $schema = null)
    {
        $result = array();
        $this->setQuery("PRAGMA table_info($table)");
        $columns = $this->loadObjectList();
        foreach ($columns as $column) {
            $result[] = (object) array(
                "TABLE_CATALOG" => "",
                "TABLE_SCHEMA" => "",
                "TABLE_NAME" => $table,
                "COLUMN_NAME" => $column->name,
                "ORDINAL_POSITION" => $column->cid,
                "COLUMN_DEFAULT" => $column->dflt_value,
                "IS_NULLABLE" => $column->notnull == 0 ? "YES" : "NO",
                "DATA_TYPE" => $column->type,
                "CHARACTER_MAXIMUM_LENGTH" => "",
                "CHARACTER_OCTET_LENGTH" => "",
                "NUMERIC_PRECISION" => "",
                "NUMERIC_SCALE" => "",
                "DATETIME_PRECISION" => "",
                "CHARACTER_SET_NAME" => "",
                "COLLATION_NAME" => "",
                "COLUMN_TYPE" => "",
                "COLUMN_KEY" => "",
                "EXTRA" => "",
                "PRIVILEGES" => "",
                "COLUMN_COMMENT" => ""
            );
        }
        return $result;
    }
}
