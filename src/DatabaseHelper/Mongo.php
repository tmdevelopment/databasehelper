<?php
namespace DatabaseHelper;

require_once __DIR__ . "/../../vendor/autoload.php";

/**
 * TMDevelopment MSSQL class
 *
 * @package    tmdevelopment.databasehelper
 * @license		GNU/GPL
 */


class Mongo extends Connection
{

    var $function_prefix = "sqlsrv";

    // an idea, COLUMN LENGTH -1 = MAX INT
    var $VARCHAR_MAX_VAL = 2147483647;

    //Connection params
    var $host = "";
    var $user = "";
    var $pass = "";
    var $db = "";

    /**
     * Cunstructor of MSSQL class
     *
     * @param string $host mysql hostname hostname|ip\servername[,port] eg  192.168.88.41\SQLEXPRESS,1234
     * @param string $user mysql username
     * @param string $pass mysql password
     * @param string $db mysql database name
     * @param string $prefix table prefix
     *
     * @access public
     */
    function __construct($host = null, $user = null, $pass = null, $db = null)
    {

        if (!class_exists("\MongoDB\Client")) {
            throw new Exception("class MongoDB\Client does not exist");
        }

        if (isset($host)) {
            $this->host = $host;
        }
        if (isset($user)) {
            $this->user = $user;
        }
        if (isset($pass)) {
            $this->pass = $pass;
        }
        if (isset($db)) {
            $this->db = $db;
        }

        $this->connect();
    }

    public function connect()
    {
        if (empty($this->connection)) {
            set_error_handler(array(parent::class, 'custom_error_handler'));
            try {

                $uri = "mongodb://$this->user:" .  urlencode($this->pass) . "@$this->host/$this->db";

                $this->connection = new \MongoDB\Client($uri);

                $database = $this->connection->selectDatabase("webshop");
                $names = $database->listCollections();


                $this->connection = sqlsrv_connect($this->host, $config);
                if (!$this->connection) {
                    $errors = sqlsrv_errors();
                    var_dump($errors);
                    die("unable to connect");
                }
            } catch (Exception $e) {
                $this->error_msg = $e->getMessage();
                $this->error = -1;
            }
            restore_error_handler();
        }
        return true;
    }
    public function query($params = array(), $options = array())
    {
        if ($this->connection) {
            $this->error_msg = null;
            $this->result = sqlsrv_query($this->connection, $this->query, $params, $options);
            if (!$this->result) {
                $this->error_msg = sqlsrv_errors();
                return false;
            }
            return true;
        } else {
            return false;
        }
    }



    public function loadObjectList()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = sqlsrv_fetch_object($this->result)) {
                $returnArray[] = $row;
            }
            sqlsrv_free_stmt($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadObject()
    {
        if ($this->query(array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET))) {
            //echo sqlsrv_num_rows($this->result);
            if (empty($this->error_msg) && sqlsrv_num_rows($this->result) != 1) {
                if (sqlsrv_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (sqlsrv_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = sqlsrv_fetch_object($this->result);
            return $ret;
        }
        return false;
    }

    public function printHTMLTable()
    {
        $this->query(array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET));
        if ($this->result && sqlsrv_num_rows($this->result)) {
            $nrow = 0;
            echo ("<table border=1 cellpadding=5 cellspacing=0><tr><th>Row</th>\n");
            $meta = sqlsrv_field_metadata($this->result);

            foreach ($meta as $field) {
                echo ("<th>" . $field["Name"] . "</th>\n");
            }
            echo ("</tr>\n");
            while ($row = sqlsrv_fetch_array($this->result)) {
                echo ("<tr><td>" . $nrow . "</td>");
                for ($i = 0; $i < count($row) / 2; $i++) {
                    echo ("<td>" . $row[$i] . "</td>\n");
                }
                echo ("</tr>\n");
                $nrow++;
            }
            echo ("</table>\n");
        } else {
            echo "<p>No data found</p>";
        }
    }

    public function getAffectedRows()
    {
        return sqlsrv_rows_affected($this->connection);
    }

    public function insertObject($table, &$object, $keyName = "")
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES ( %s ) ';
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $values[] = $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(",", $fields), implode(",", $values));

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            var_dump($this->error_msg);
            return false;
        }
        if (!empty($keyName)) {
            $object->$keyName = $this->insertId();
        }
        return true;
    }

    public function updateObject($table, &$object, $keyName = "")
    {

        if (empty($keyName)) {
            return false;
        }

        $fmtsql = 'UPDATE ' . $this->nameQuote($table) . ' SET  %s  WHERE ' . $this->nameQuote($keyName) . ' = ' . $this->quote($object->$keyName);
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if (is_array($v) or is_object($v) or $v === null) {
                continue;
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = " " . $this->nameQuote($k) . "=" . $this->quote($v) . " ";
        }



        $fmtsql = sprintf($fmtsql, implode(",", $fields));

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            var_dump($this->error_msg);
            return false;
        }
        if (!empty($keyName)) {
            $object->$keyName = $this->insertId();
        }
        return true;
    }

    public function insertId()
    {
        // TODO: SELECT IDENTITY
        $this->setQuery('SELECT @@IDENTITY');

        return (int) $this->loadResult();
    }

    public function loadResult()
    {
        $ret = null;
        if ($this->query()) {
            if ($row = sqlsrv_fetch_array($this->result, SQLSRV_FETCH_NUMERIC)) {
                $ret = $row[0];
            }
            return stripslashes($ret);
        }
        return false;
    }
}
