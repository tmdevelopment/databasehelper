<?php

namespace DatabaseHelper;

/**
 * TMDevelopment MySQLi class
 *
 * @package    tmdevelopment.databasehelper
 * @license		GNU/GPL
 */
class MySQLi extends Connection
{

    var $function_prefix = "mysqli";

    //Connection params
    var $host = "";
    var $user = "";
    var $pass = "";
    var $db = "";
    var $prefix = "";

    private $types = array(
        1 => 'int', //'tinyint',
        2 => 'int', //'smallint',
        3 => 'int',
        4 => 'float',
        5 => 'double',
        7 => 'timestamp',
        8 => 'bigint',
        9 => 'mediumint',
        10 => 'date',
        11 => 'string', //'time',
        12 => 'datetime',
        13 => 'year',
        16 => 'bit',
        252 => 'string',
        //252 is currently mapped to all text and blob types (MySQL 5.0.51a)
        253 => 'string', //'varchar',
        254 => 'char',
        246 => 'double' //decimal
    );


    public $dateTimeAsString = true;
    public $serialize = false;
    public $jsonEncode = false;

    /**
     * Cunstructor of MySQLi class
     *
     * @param string $host mysql hostname eg localhost, 127.0.0.1
     * @param string $user mysql username
     * @param string $pass mysql password
     * @param string $db mysql database name
     * @param string $prefix table prefix
     *
     * @access public
     */
    function __construct($host = null, $user = null, $pass = null, $db = null, $prefix = null)
    {
        if (!function_exists("mysqli_connect")) {
            throw new \Exception("function mysqli_connect does not exist");
            exit();
        }
        if (isset($host)) {
            $this->host = $host;
        }
        if (isset($user)) {
            $this->user = $user;
        }
        if (isset($pass)) {
            $this->pass = $pass;
        }
        if (isset($db)) {
            $this->db = $db;
        }

        if (isset($prefix)) {
            $this->prefix = $prefix;
        }
        $this->isConnected = $this->connect();
    }

    /**
     *
     * @return boolean
     *
     * @access private
     */
    private function connect()
    {

        $result = true;

        if (empty($this->connection)) {
            set_error_handler(array(parent::class, 'custom_error_handler'));
            try {
                $this->connection = mysqli_init();
                mysqli_options($this->connection, MYSQLI_OPT_CONNECT_TIMEOUT, 10);
                mysqli_real_connect($this->connection, $this->host, $this->user, $this->pass);
                mysqli_set_charset($this->connection, 'utf8');
                mysqli_select_db($this->connection, $this->db);

                $this->serverVersion = mysqli_get_server_version($this->connection);
            } catch (\Exception $e) {
                $this->error_msg = $e->getMessage();
                $this->error = -1;
                $result = false;
            }
            restore_error_handler();
        }
        return $result;
    }

    public function nameQuote($name)
    {
        $name = explode(".", $name);
        foreach ($name as $key => $namePart) {
            $name[$key] = "`" . $namePart . "`";
        }
        return implode(".", $name);
    }

    /**
     * Method for querying previously set query
     *
     * @access public
     * @return boolean result of querying
     */
    public function query()
    {
        if ($this->connection) {
            $this->error_msg = null;
            $this->result = mysqli_query($this->connection, $this->query);
            if (!$this->result) {
                $this->error_msg = mysqli_error($this->connection);
                $this->error = mysqli_errno($this->connection);
                try {
                    // lets add some log4php here
                    //  $this->error . " - " . $this->error_msg . " - " . $this->query
                } catch (\Exception $ex) {
                }
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method for loading all the rows as array of objects
     *
     * @access public
     * @return mixed, false if query failed, on success array
     */
    public function loadObjectList($class = null)
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = mysqli_fetch_object($this->result)) {
                if($class){
                    $returnArray[] = new $class($row);
                }
                else{
                    $returnArray[] = $row;
                }
            }
            mysqli_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadAssocList()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = mysqli_fetch_assoc($this->result)) {
                $returnArray[] = $row;
            }
            mysqli_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    public function loadResultArray()
    {
        if ($this->query()) {
            $returnArray = array();
            while ($row = mysqli_fetch_row($this->result)) {
                $returnArray[] = $row[0];
            }
            mysqli_free_result($this->result);
            return $returnArray;
        }
        return false;
    }

    /**
     * Method for loading first value of query statement. e.g. "SELECT 1, 2, 3" 1 will be returned
     *
     * @return mixed, boolean false if query fails, on success first value
     */
    public function loadResult()
    {
        if ($this->query()) {
            $row = mysqli_fetch_row($this->result);
            if ($row !== null) {
                return $row[0];
            }
        }
        return null;
    }

    /**
     * Method for loading single object (row)
     *
     * @access public
     * @return mixed, boolean false if query fails or row count does not equal 1, on success object
     */
    public function loadObject($setTypes = false, $class = null)
    {
        if ($this->query()) {
            if (empty($this->error_msg) && mysqli_num_rows($this->result) != 1) {
                if (mysqli_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (mysqli_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = mysqli_fetch_object($this->result);
            if ($setTypes) {

                if($class != null){
                    $ret = new $class(get_object_vars($ret));
                }
                else{

                    for ($i = 0; $i < count(get_object_vars($ret)); $i++) {
                        $r = mysqli_fetch_field_direct($this->result, $i);
                        $type = $this->types[$r->type];
                        if ($ret->{$r->name} !== null) {
                            if ($this->dateTimeAsString && $type == "datetime") {
                                $type = "string";
                            }
                            switch ($type) {
                                case "datetime":
                                    $ret->{$r->name} = new \DateTime($ret->{$r->name});
                                    break;
                                case "date":
                                    $ret->{$r->name} = $ret->{$r->name};
                                    break;
                                default:
                                    settype($ret->{$r->name}, $type);
                                    break;
                                
                            }
                        }
                    }
                }
            }

            return $ret;
        }
        return false;
    }


    /**
     * Method for loading single array (row)
     *
     * @access public
     * @return mixed, boolean false if query fails or row count does not equal 1, on success object
     */
    public function loadAssoc()
    {
        if ($this->query()) {
            if (empty($this->error_msg) && mysqli_num_rows($this->result) != 1) {
                if (mysqli_num_rows($this->result) == 0) {
                    $this->error_msg = "Zero rows found";
                } else if (mysqli_num_rows($this->result) > 1) {
                    $this->error_msg = "More than 1 row found";
                }
                return false;
            }
            $ret = mysqli_fetch_assoc($this->result);
            return $ret;
        }
        return false;
    }



    public function printHTMLTable()
    {
        $this->query();
        if ($this->result && mysqli_num_rows($this->result)) {
            $nrow = 0;
            echo ("<table border=1 cellpadding=5 cellspacing=0><tr><th>Row</th>\n");
            $finfo = mysqli_fetch_fields($this->result);
            foreach ($finfo as $val) {
                echo ("<th>" . $val->name . "</th>\n");
            }
            echo ("</tr>\n");

            echo ("</tr>\n");
            while ($row = mysqli_fetch_array($this->result)) {
                echo ("<tr><td>" . $nrow . "</td>");
                for ($i = 0; $i < mysqli_num_fields($this->result); $i++)
                    echo ("<td>" . $row[$i] . "</td>\n");
                echo ("</tr>\n");
                $nrow++;
            }
            echo ("</table>\n");
        } else {
            echo "<p>No data found</p>";
        }
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->connection);
    }

    public function insertObject($table, &$object, $keyName = null)
    {
        return $this->insertOrUpdateObject($table, $object, $keyName, null, false);
    }

    public function insertOrUpdateObject($table, &$object, $keyName = null, $doNotUpdate = array() /* fields that will not be updated */, $update = true)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES ( %s ) ' . ($update ? ' ON DUPLICATE KEY UPDATE' : '');
        $fields = array();
        foreach (get_object_vars($object) as $k => $v) {
            if($v instanceof \DateTime){
                $v = $v->format("Y-m-d H:i:s");
            }
            if(is_bool($v)){
                $v = $v ? 1 : 0;
            }
            if($v === null) {
                continue;
            }
            if (is_array($v) or is_object($v)) {
                if($this->serialize){
                    $v = $this->escape(serialize($v));
                }
                else if($this->jsonEncode){
                    $v = $this->escape(json_encode($v, JSON_UNESCAPED_UNICODE));
                }
                else{
                    continue;
                }
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $values[] = $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(",", $fields), implode(",", $values));

        if ($update) {
            // quotes values
            for ($j = 0; $j < count($doNotUpdate); $j++) {
                $doNotUpdate[$j] = $this->nameQuote($doNotUpdate[$j]);
            }
            for ($i = 0; $i < count($fields); $i++) {
                if (!in_array($fields[$i], $doNotUpdate)) {
                    if ($i != 0) {
                        $fmtsql = $fmtsql . ",";
                    }
                    $fmtsql = $fmtsql . " " . $fields[$i] . "=" . $values[$i];
                }
            }
        }

        $this->setQuery($fmtsql);
        if (!$this->query()) {
            if ($this->debug) {
                //var_dump($this->query, $this->error_msg);
            }
            return false;
        }
        if ($this->getAffectedRows() > 0 && empty($object->$keyName) && !empty($keyName)) {
            $object->$keyName = $this->insertId();
        }
        return true;
    }

    public function insertId()
    {
        return mysqli_insert_id($this->connection);
    }

    public function bulkInsertOrUpdateObject($table, $objects, $objectsPerQuery = 100)
    {
        $fmtsql = 'INSERT INTO ' . $this->nameQuote($table) . ' ( %s ) VALUES (%s) ON DUPLICATE KEY UPDATE %s';
        $fields = array();
        $update_fields = array();
        foreach (get_object_vars($objects[0]) as $k => $v) {
            if($v instanceof \DateTime){
                $v = $v->format("Y-m-d H:i:s");
            }
            if(is_bool($v)){
                $v = $v ? 1 : 0;
            }
            if($v === null) {
                continue;
            }
            if (is_array($v) or is_object($v)) {
                if($this->serialize){
                    $v = $this->escape(serialize($v));
                }
                else if($this->jsonEncode){
                    $v = $this->escape(json_encode($v, JSON_UNESCAPED_UNICODE));
                }
                else{
                    continue;
                }
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fields[] = $this->nameQuote($k);
            $update_fields[] = $this->nameQuote($k) . "=VALUES(" . $this->nameQuote($k) . ") ";
        }
        $values_arr = array();
        $mitmes = 0;
        foreach ($objects as $obj) {
            $mitmes++;
            $values = array();
            foreach (get_object_vars($obj) as $k => $v) {
                if($v instanceof \DateTime){
                    $v = $v->format("Y-m-d H:i:s");
                }
                if(is_bool($v)){
                    $v = $v ? 1 : 0;
                }
                /*
                if($v === null) {
                    continue;
                }
                */
                if (is_array($v) or is_object($v)) {
                    if($this->serialize){
                        $v = $this->escape(serialize($v));
                    }
                    else if($this->jsonEncode){
                        $v = $this->escape(json_encode($v, JSON_UNESCAPED_UNICODE));
                    }
                    else{
                        continue;
                    }
                }
                if ($k[0] == '_') { // internal field
                    continue;
                }
                $values[] = $this->Quote($v);
            }
            $values_arr[] = join(",", $values);
            if ($mitmes == $objectsPerQuery) {
                $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
                $this->setQuery($sql);
                if (!$this->query()) {
                    //var_dump($this);
                    //echo $sql;
                }
                $mitmes = 0;
                $values_arr = array();
            }
        }
        if ($mitmes != 0 && $mitmes != $objectsPerQuery) {
            $sql = sprintf($fmtsql, join(", ", $fields), join("), (", $values_arr), join(",", $update_fields));
            $this->setQuery($sql);
            if (!$this->query()) {
                //var_dump($this);
                //echo $sql;
            }
        }
    }

    public function updateObject($table, &$object, $keyName = null)
    {
        $fmtsql = 'UPDATE ' . $this->nameQuote($table) . ' SET %s  WHERE ' . $this->nameQuote($keyName) . ' = ' . (is_numeric($object->$keyName) ? $object->$keyName : $this->quote($object->$keyName));
        $fieldValuePairs = array();
        foreach (get_object_vars($object) as $k => $v) {
            if($v instanceof \DateTime){
                $v = $v->format("Y-m-d H:i:s");
            }
            if(is_bool($v)){
                $v = $v ? 1 : 0;
            }
            if($v === null) {
                continue;
            }
            if (is_array($v) or is_object($v)) {
                if($this->serialize){
                    $v = $this->escape(serialize($v));
                }
                else if($this->jsonEncode){
                    $v = $this->escape(json_encode($v, JSON_UNESCAPED_UNICODE));
                }
                else{
                    continue;
                }
            }
            if ($k[0] == '_') { // internal field
                continue;
            }
            $fieldValuePairs[] = $this->nameQuote($k) . "=" . $this->quote($v);
        }
        $fmtsql = sprintf($fmtsql, implode(", ", $fieldValuePairs));
        $this->setQuery($fmtsql);
        if (!$this->query()) {
            //var_dump($this->error_msg);
            return false;
        }
        return true;
    }

    public function setDebug($value)
    {
        $this->debug = (bool) $value;
    }
}
